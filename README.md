# After Everything I’ve Been Through in Another World, All I Can Do is Code with My Advanced Programming Knowledge

You walk slowly through the empty battlefield. No one else here beside you and the sword of the fallen magic knight. Burnt remains decorated the landscape because of Aku Ganteng’s power.  Almost no sound can be heard. Only the sound of the wind that blows slowly around the field. 

The field now looks like a tomb of the swords. The lonely swords are pierced to the ground, having lost their owners in the battle. As you walk around them, you feel regret. But as a king, a ruler, this sacrifice has to be made. 

Your eyes gaze into the beauty of the former body of Aku Ganteng. Finally such a terrifying opponent is defeated. You feel relieved and also regret after knowing the truth. He was once a hero. He always hated the dark side of the human race. He swore to destroy all hatred in humans. He is willing to take any risk to fulfill that goal.  But in the end, he failed to do so. Human nature is too ugly in the first place. In his despair, he chose to accept the fact. In that state, he chose to take all the hatred, all lies, and all evils in the world rather than gracefully defeat it as a hero should do. He chose to become a demon in order to become a hero. 

In the end, he didn’t get what he aimed for.  No one remembers his name. No one acknowledges his heroism. And finally when he took all the evils in the world, everyone hated him. The world has betrayed him. At his final moment, the world that he wants to save so badly didn’t even bother to save its saviour. As if his heroism was just an illusion. All you can do now to end his suffering is to end his life and set him free from his duty. 

“ Rest in peace.. “

A.I stands close to where you are, still alert for a chance that some enemies are still alive. She gazes into your body as if something is weird about it. 

“ Is there something strange about my body A.I?”

As she worriedly looks into your eyes, you can’t help to ask her a question. However she does not answer immediately. She just stands there without saying a word. After a moment, you finally realize why she was looking at you profusely. Your body starts to become invisible. 

“ I know.. “ you replied. 

You know your existence is starting to become weaker. You just destroyed the magic power, the one that bound you to this world. Of course with that power gone, you no longer have the right to stay in this world. 
The humans of this world must start to learn how to use science. Especially this empire that you had led,  you want it’s people to know how to properly use programming. Almost all knowledge you know has been passed to A.I, except one thing. The final gift from you. 

“ Listen A.I. From this moment, I want you to become the king. To lead this empire in this world without magic. You must become the example of how to live a life without magic. Teach them how to use it properly. “

“ So this moment has to come.  [] (empty list) trusted you, so I will accept everything you order me to do. Even if it hurts to see you go, as you always say, what has to be done, must be done. I willingly accept this role and responsibility. “

“ Thank you. “

A.I smiles, but you know she is trying very hard not to cry. Of course, this is the second time a person that important to her will leave her behind. She believes that her life will be better with you. However this is beyond your control. You don’t have other choices, other than to accept the bitter reality. 

She is just an AI but ironically, she understands feelings of others more than a human does. You thought [] has created a wonderful AI. You believe this world will be alright in her hands.  

“ A.I, I will teach you one last thing. Let’s go back to the palace. “

“ Yes Sir! “

****

As you don’t have much time left, you start to explain your last gift: Docker. You feel bad for A.I, since she just returned from battle, but the condition forces you to do so. 

“ Have you heard about Docker? “

“ Other than the words that you utter in your sleep, I haven't.”

“ I see. “

You feel the way she learned the word is quite… embarrassing. But that doesn't matter. 

“ Perhaps, you have been familiar with container based applications? This time I want to show you how to utilize it. Do you know the motivations?  “

“ As I remember what you have told me, when we deploy applications in a traditional way, we have to install everything that the application needs manually. For example when deploying a java application with a gradle automation system, of course we need to install java and the gradle itself. In other words, the developer needs to know everything about the application and install it manually. With container based, you can define all the dependencies that the application needs in the container. The developer just needs to configure port and networking rather than manually doing everything. “

“ Impressive. That makes it fast. As you have known the basic concept, the rest is to know the technicals. I will start from the basics of Docker. “

## Docker

Docker itself is a tool used to ease the development of applications by creating, running, and deploying them using containers. Containers allow a developer to package up an application and all its dependencies, and deploy it as one package. The container will ensure the application will run on other machines that support Docker regardless of customized settings and such that differ from machine to machine.


### Creating a Dockerfile

To create a Docker image, first we need to create a Dockerfile. A Dockerfile (no extension) is a simple text document containing all the commands to build an image. 

The format of a Dockerfile is as follows:

```
#Comment
INSTRUCTION arguments
```

You can find a comprehensive Dockerfile documentation [here](https://docs.docker.com/engine/reference/builder/)

Hint: Since we are using gradle and building a jar file, the instructions and arguments in your Dockerfile will be for building a jar file.

*Checklist*

- [ ] Create a Dockerfile (mandatory)

### Making an image from Dockerfile

After the Dockerfile is complete, you can now build the image with the Dockerfile using the command `docker build`. 

After building the image, now you can try to create a container for that image. To create a container, you will need to use the command `docker container create` with the arguments necessary (such as name, network, and environment variables if needed).

*Checklist*
- [ ] Build your Dockerfile with `docker build` (mandatory)
- [ ] Create a container for your new image with `docker container create` (mandatory)

### Pushing the image to DockerHub

The next step is to push the image you have built to [DockerHub](https://hub.docker.com/).
You will need to create an account for DockerHub. After you have done that, push your application image to DockerHub. You will need this image to run the container on Portainer. (Hint: You might need to know about docker login command). 

*Checklist*
- [ ] Create a DockerHub account (mandatory)
- [ ] Push your image to DockerHub (mandatory)

## Making the container run on Portainer

You can also create a container in Portainer.io (with the link provided in the Scele). There you will see an endpoint named local. Click the endpoint until you can see the dashboard, then go to the container option and create a new container. This container will be for the image you have pushed to DockerHub, so set the environment variables and other settings so that the image you have pushed will run on Portainer. (Hint: Understanding the Docker network structure may help in doing this task).

*Checklist*
- [ ] Log in to your Portainer.io account (mandatory)
- [ ] Create a new container in the local endpoint (mandatory)
- [ ] Configure the container so that it can run the image you have pushed to DockerHub (mandatory)

## Docker Compose (Additional)
By using Docker Compose, multiple Docker containers can communicate with each other. Every container used will be declared inside a file named `docker-compose.yml`. You can check Docker Compose documentation in the Docker [here](https://docs.docker.com/compose/)

Docker Compose needs separate installation. You can check its documentation [here](https://docs.docker.com/compose/install/)

Notes : You must finish the mandatory task first before doing additional tasks. If the mandatory tasks aren’t done completely, any additional task done won’t be graded

*Checklist*
- [ ] Create the file `docker-compose.yml` (additional)
- [ ] Write the file (`docker-compose.yml`) with configuration needed for this project (additional)
- [ ] Run the docker-compose (additional)

# The end of the journey

You have given this empire your last gift. You also have written a letter for A.I as you left her in the middle of the night. You want to return to the place that is the beginning of everything in this world for you. You feel you won’t have another chance if you do not do it now. 

You take a trip to the Magic Association. As your body becomes more and more invisible, you think you can give a final word to The Manager and also a final look to your old room. The trip took some days. Along the way, you always hope you will make it in time. Fortunately, unlike your last trip,  you are able to use the faster route because war is over. Outside the windows, your eyes are gazing into memorable places. Finally the journey will come to an end and everything will be a memory. 

“ I am home, Manager. “

As you arrive at the Magic Association, you visit the grave of your former boss.  Unfortunately, the battle has cost The Manager’s life. You placed a rose in the grave before entering the main building. 

In your old room that hasn’t been taken care of for some time, you sit at your old chair that brings old memories. When you remember them, you smile. Some of them are happy moments, some of them are frightening, and some of them are sad. But now, all of them are precious to you. 

With some parts of your body now have gone, you know that is the end of your journey. You feel relieved because you can end it in the place where everything began. In your last moments, you see a letter.  You are wondering who sent it. The name of the sender is The Assistant. 

You can’t believe what your eyes tell you. The Assistant should have been dead by now. You saw the Assistant’s death in front of your own eyes. There is no way the Assistant is still alive. You open the letter. 

```
Congratulations. I am grateful that you are alright, my partner. 
Your journey ends here. But you know it is not over yet. 
This a new beginning of your long story in your world. If fate approves, 
we will meet again soon, in your world. 

Sincerely,


The Assistant 


```

When you have finished reading the letter, your body is completely gone and taken back to your original world. Your journey here is not the best if you may say, but it wasn’t bad. You know all the things you have experienced will help you in the future. This is not the end; It is the beginning of a bigger journey. After all you are not the hero who has the power of heaven, power that can change the world. In the end, you saved the world with your own power, with your knowledge of programming. You already understand, that is your role.  After everything you’ve been through in another world, all you can do is code with your advanced programming knowledge. 

*- The End -*

