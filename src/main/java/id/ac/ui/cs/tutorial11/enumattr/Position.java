package id.ac.ui.cs.tutorial11.enumattr;

public enum Position {
    NORTH, EAST, WEST, SOUTH;

    public static Position getInstance(String type){
        if(type.equals("north")){
            return Position.NORTH;
        } else if(type.equals("east")){
            return Position.EAST;
        } else if(type.equals("west")){
            return Position.WEST;
        } else {
            return Position.SOUTH;
        }
    }
}