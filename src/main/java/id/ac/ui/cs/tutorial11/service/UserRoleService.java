package id.ac.ui.cs.tutorial11.service;

import id.ac.ui.cs.tutorial11.model.UserModel;

public interface UserRoleService {
    UserModel addUser(UserModel user);
    public String encrypt(String password);

}
